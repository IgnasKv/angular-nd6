import { Component, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  @ViewChild('form') signUpForm: NgForm;
  defaultSub: string = "advanced";
  submitted: boolean = false;
  user = {
    email: "",
    subscription: "",
    password: ""
  }

  onSubmit() {
    this.submitted = true;
    console.log(this.signUpForm);
    this.user.email = this.signUpForm.value.mail;
    this.user.subscription = this.signUpForm.value.subscription;
    this.user.password = this.signUpForm.value.password;
    this.signUpForm.resetForm();
  }
}
